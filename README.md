# Planetz API wih react

NASA provides a public API where you can make http calls and get information about:

+ Nearby asteroids
+ Space weather
+ Earth data
+ Exoplanets data
+ And more...!

To get information about their service go to [NASA API](https://api.nasa.gov/).


## About 

Here I create a react app to consume their API using the following libraries:

+ axios
+ MaterialUI
+ react router dom
  
As backend I use node.js for the server and mongodb for the nosql database (hosted at MongoAtlas).

Functionality:

+ Using a cronjob defined in the _server.js_ file I scrappe the www.ibtimes.com everyday at 1am to fetch the 9 latest news and store them at mongodb database.
+ The react application in the home page makes a call to the node server to fetch the news stored at the database.
+ In the _nasa api_ section a set of images are fetched from the nasa api and displayed using pagination.


## How 2 install it locally?

Make sure you have __yarn__ package manager and install the depencies listed in packages.json by running:
```sh
$ yarn install
$ yarn start
```


## Deployment

I did the deployment using Heroku, a PaaS system where it's easier to deploy applications just by writing a configuration file
called __Procfile__.



## Links of interest

[Heroku](www.heroku.com)