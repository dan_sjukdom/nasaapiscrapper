FROM node:14.15.3-alpine3.12

WORKDIR /app

COPY package.json .

RUN yarn install

EXPOSE 8000

COPY . .

CMD ["yarn", "start"]