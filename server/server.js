const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const path = require("path");
const cors = require("cors");
const axios = require("axios");
const JSSoup = require("jssoup").default;
const cron = require("node-cron");
const { MongoClient } = require("mongodb");
const port = process.env.PORT || 8000;


// MongoDB client connection
const uri = "mongodb+srv://sjkdm:nYRQbapTzcKzDiCl@sjkdmclooster.ay8i0.mongodb.net/nasa_scrapper?retryWrites=true&w=majority";
const client = new MongoClient(uri, {
    useNewUrlParser: true,    
    useUnifiedTopology: true 
});

client.connect();


// Express app
const app = express();


// Middleware
app.use(cors());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(morgan("dev"));
// Serve static content
app.use(express.static(path.join(__dirname, "../build")))



app.get("/api/nasa/news/", async (req, res, next) => {    
    try {
        let articles = [];
        const cursor = client.db("nasa_scrapper").collection("articles").find({}).toArray((err, result) => {
            res.json(result.slice(0, 9));
        });
    }
    catch (err) {
        next(err);
    }
});


// Schedule job: fetch the news everyday at 1am
/*
const base_url = "https://www.ibtimes.com";
cron.schedule("0 1 * * *", async () => {
    try {
        const res = await axios.get(`${base_url}/search/site/nasa`);
        if (res.status === 200) {

            const html = res.data;
            let soup = new JSSoup(html);
            let articles_html = soup.findAll("article").slice(0, 9);

            let articles = [];
            for (let i=0; i< articles_html.length; i++) {

                let href = articles_html[i].find("a").attrs.href;
                let article_title = articles_html[i].find("h3").text;
                let article_summary = articles_html[i].find("div", "summary").text;
                let article_published = articles_html[i].find("div", "byline").text;

                articles.push({
                    title: article_title,
                    href: base_url + href,
                    published: article_published,
                    summary: article_summary
                })
                // let promise_body = axios.get(base_url + href)
                // .then(res => {
                //     soup = new JSSoup(res.data);
                //     let body = soup.find("div", "article-body");
                //     let article_body = "";
                //     body.findAll("p").map(ptag => {
                //         article_body += ptag.text;
                //     })
                //     return article_body;
                // })
                // .catch (err => console.log(err))

                // promise_body.then(article_body => { 
                //     return article_body;
                // }) 

                // console.log("LIST")
                // console.log(articles)
                
            }
            try { 
                const collection = client.db("nasa_scrapper").collection("articles");
                await collection.insertMany(articles);
            } 
            catch (err) {
                console.log(err);
            }
            finally {
                console.log("Articles were inserted to the database");
            }
        }
        else {
            console.log("There was an error making the call to www.ibtimes.com");
        }
    }
    catch (err) {
        throw new Error(err)
    }
});
*/

app.get("/*", (req, res) => {
    res.sendFile(path.join(__dirname, "../build"));
});

app.listen(port, "0.0.0.0", (req, res) => {
    console.log(`Listening on port ${port}`);
});
