const axios = require("axios");
const cheerio = require("cheerio");
const { response } = require("express");
const JSSoup = require("jssoup").default;


const base_url = "https://www.ibtimes.com";

const fetchNews = async response => {
    try {
        const res = axios.get(`${base_url}/search/site/nasa`);
        res.then(c => {
        // if (res.status === 200) {

            const html = res.data;
            let soup = new JSSoup(c.data);
            let articles = [];
            let articles_html = soup.findAll("article").slice(0, 9);

            for (let i=0; i<= articles_html.length; i++) {
                let href = articles_html[i].find("a").attrs.href;
                let article_title = articles_html[i].find("h3").text;
                console.log(article_title.contents)
                let article_summary = articles_html[i].find("div", "summary").text;
                let article_published = articles_html[i].find("div", "byline").text;
                articles.push({
                    title: article_title,
                    href: base_url + href,
                    published: article_published,
                    summary: article_summary
                })
                // let promise_body = axios.get(base_url + href)
                // .then(res => {
                //     soup = new JSSoup(res.data);
                //     let body = soup.find("div", "article-body");
                //     let article_body = "";
                //     body.findAll("p").map(ptag => {
                //         article_body += ptag.text;
                //     })
                //     return article_body;
                // })
                // .catch (err => console.log(err))

                // promise_body.then(article_body => { 
                //     return article_body;
                // }) 

                // console.log("LIST")
                // console.log(articles)

            }

            response.json(articles);
        }
        // else {
        //     console.log("There was an error making the call to www.ibtimes.com");
        // }
    })
    // catch (err) {
    //     throw new Error(err);
    // }
}


exports.nasaNewsController = (req, res) => {
    // fetchNews();
    console.log("scrapping...")
    fetchNews(res)
    // console.log("I called articles")
    // articles.then(articles => {
    //     res.json(articles)
    // });
    // console.log("teh end...")

};