import SearchBar from './SearchBar';
import Header from './Header';
import "../../assets/css/Home.css";
import Content from "./Content";
import {
    SearchProvider
} from './SearchContext';


const Home = () => {
    
    return (
        <SearchProvider>
            <div className="Home">
                <Header />
                {/* <SearchBar /> */}
                <Content />
            </div>
        </SearchProvider>
    )
};

export default Home;