import { useEffect, useState, useContext } from 'react';
import {
    TextField,
    InputAdornment,
    IconButton,
    Button
} from '@material-ui/core';
import {
    makeStyles, withStyles
} from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import axios from 'axios';
import '../../assets/css/Search.css';
import { SearchContext } from './SearchContext';



const useStyles = makeStyles(theme => ({
    search_bar__root: {
    },
    search_bar__input: {
        color: "white",
        textTransform: "capitalize"
    },
    input__outlines: {
        color: "red"
    }
}));


const CustomTextField = withStyles({
    root: {
        "& label.Mui-focused": {
            color: "#7951E6",
        },
        "& .MuiOutlinedInput-root": {
            borderRadius: "25px",
            // border: "1px solid white",
            "&.Mui-focused fieldset": {
                borderColor: "#7951E6"
            },
        },
        "& .MuiOutlinedInput-input": {
            color: "gray"
        },
        "& .MuiInputLabel-root": {
            color: "gray",   
        },
    }
})(TextField);


const SearchBar = () => {

    const classes = useStyles();
    const [state, dispatch ] = useContext(SearchContext);

    const handleSearch = e => {
        e.preventDefault();
        dispatch({"type": "SET_QUERY", "data": e.target.value});
    };

    const handleSearchButton = e => {
        e.preventDefault();
        if (state.query !== "") {
            console.log("making the api call...")
        }
    }


    return (
        <form className="search__form" method="GET" autoComplete="off">
            <CustomTextField
                variant="outlined"
                value={state.query}
                onChange={handleSearch}
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start">
                            <IconButton onClick={handleSearchButton}>
                                <SearchIcon style={{ color: "gray" }}/>
                            </IconButton>
                        </InputAdornment>
                    )
                }}
            />
        </form>
    )
};

export default SearchBar;