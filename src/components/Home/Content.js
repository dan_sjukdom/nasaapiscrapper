import { useEffect, useState } from 'react';
import {
    Typography,
    Card,
    CardContent,
    CircularProgress,
    makeStyles,
    CardActionArea,
    Grid,
    Container
} from '@material-ui/core';
import axios from 'axios';


const useStyles = makeStyles(theme => ({
    progress__root: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    grid__container: {

    },
    main__title: {
        textAlign: "center",
        fontFamily: "Roboto Mono, monospace",
        margin: "30px 30px",
        color: "white"
    },
    main__subtitle: {
        textAlign: "center",
        fontFamily: "Roboto Mono, monospace",
        color: "white",
        padding: "30px"
    },
    card__root: {
        height: "370px",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        backgroundColor: "rgba(39, 39, 39, 0.7)",
        color: "white",
        overflowY: "auto"
    },
    card__action: {
        height: "inherit",
    },
    card__text: {
        fontFamily: "Roboto Mono, monospace",
        textAlign: "justify",
        fontSize: "14px"
    },
    card__title: {
        fontFamily: "Roboto Mono, monospace",
        textAlign: "center",
        fontSize: "18px"
    },
    card__published: {
        fontFamily: "Roboto Mono, monospace",
        textAlign: "center",
    },
}));


const Content = () => {

    const classes = useStyles();
    const [ loading, setLoading ] = useState(true);
    const [ data, setData ] = useState([]);
    
    useEffect(() => {
        const fetchNews = async () => {
            setLoading(true);
            setData(false);
            try {
                const response = await axios.get("api/nasa/news");
                if (response.status === 200) {
                    const news = response.data;
                    setData(news);
                    setLoading(false);
                }
            }
            catch (err) {
                throw new Error(err);
            }
        };
        fetchNews();
    }, []);

    return (
        <Container>
            {
                loading & !data? (
                    <div className={classes.progress__root}> 
                        <CircularProgress color="secondary"/>
                    </div>
                ) : (
                    <div>
                        <Typography variant="h3" className={classes.main__title}>
                            Latest NASA News
                        </Typography>
                        <Typography variant="body2" className={classes.main__subtitle}>
                            The news are scrapped from www.ibtimes.com
                        </Typography>
                        <Grid container spacing={3} className={classes.grid__container}>
                            {data.map((new_item, key) => (
                                <Grid item xs={12} md={6} lg={4} key={key}>
                                    <Card classes={{
                                        root: classes.card__root
                                    }}>
                                        <CardActionArea
                                            className={classes.card__action}
                                            onClick={() => window.open(new_item.href, "_blank")}
                                        >
                                            <CardContent>
                                                <Typography className={classes.card__title} variant="body2">
                                                    { new_item.title.replace(/&#039;/g, "'") }
                                                </Typography>
                                                <br />
                                                <Typography className={classes.card__published} variant="body2">
                                                    { new_item.published }
                                                </Typography>
                                                <br />
                                                <Typography className={classes.card__text} variant="body2">
                                                    { new_item.summary.trim().replace(/&#039;/g, "'").replace(/^\w/, (c) => c.toUpperCase())  }
                                                </Typography>
                                            </CardContent>
                                        </CardActionArea>
                                    </Card>
                                </Grid>
                            ))}
                        </Grid>
                    </div>
                )
            }
        </Container>
    )
};


export default Content;