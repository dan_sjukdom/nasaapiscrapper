import React, { useEffect, useState } from 'react';
import {
    CircularProgress,
    Typography,
    Button,
    Popper
} from '@material-ui/core';
import {
    makeStyles
} from '@material-ui/core/styles';
import axios from 'axios';
import { API_KEY } from '../../helpers/api'; 


const base_url = `https://api.nasa.gov/planetary/apod?api_key=${API_KEY}`;


const formatDate = date => {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();

    return `${year}-${month}-${day}`;
};


const useStyles = makeStyles(theme => ({
    container: {
        height: "40vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "left top",
        animation: "$slider 30s infinite alternate"
    },
    button__root: {
        backgroundImage: "linear-gradient(to right top, #8358e5, #7a52e6, #704de8, #6547e9, #5842eb)"
    },
    button__label: {
        color: "white",
        fontFamily: "Roboto Mono, monospace"
    },
    item: {
        margin: "15px 15px"
    },
    popper: {
        width: "50%",
        height: "310px",
        padding: "15px 15px",
        marginTop: "15px",
        backgroundColor: "#212121",
        overflowY: "auto",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    text: {
        textAlign: "justify",
        color: "white",
        fontFamily: "Roboto Mono, monospace"
    },
    title: {
        backgroundImage: "linear-gradient(to right top, #8358e5, #7a52e6, #704de8, #6547e9, #5842eb)",
        borderRadius: "5px",
        textAlign: "center",
        fontFamily: "Roboto Mono, monospace",
        padding: "5px 5px",
        color: "white",
    },
    image: {
        backgroundPosition: "200px 200px",
    },
    "@keyframes slider": {
        "from": {
            backgroundPosition: "left top"
        },
        "to": {
            backgroundPosition: "left bottom"
        }
    }
}));



const Header = () => {

    const classes = useStyles();
    const [isLoading, setIsLoading] = useState(true);
    const [anchorElPopper, setAnchorElPopper] = useState(false);
    const [state, setState] = useState({
        image: "",
        description: "",
        title: ""
    });
    const popper_open = anchorElPopper;

    const fetch_image_of_the_day = async () => {

        const date = formatDate(new Date(2020, 11, 12));
        const url = `${base_url}`;
    
        try {
            const response = await axios.get(url);
            if (response.status === 200) {
                const data = response.data;
                console.log(data)
                console.log(data.url)
                setIsLoading(false);
                setState({
                    ...state,
                    image: data.url,
                    description: data.explanation,
                    title: data.title
                })
            }
            else {
                setState({...state, image: null})
            }
        }
        catch (error) {
            throw new Error("Can't make the http call to NASA: ", error);
        }
    };

    

    useEffect(() => {
        fetch_image_of_the_day();
    }, []);

    return (
        <div 
            className={classes.container} 
            style={state.image? { backgroundImage: "url(" + state.image + ")" } : {}}
        >
            {
                !state.image && isLoading? (
                    <div className={classes.item}>
                        <CircularProgress color="secondary"/>
                    </div>
                ): ( 
                    <React.Fragment>
                        <div className={classes.item}>
                            <Typography variant="h6" className={classes.title}>  
                                { state.title }
                            </Typography>
                        </div>
                        <div className={classes.item}>
                            <Button 
                                variant="outlined"
                                onClick={(e) => {
                                    setAnchorElPopper(anchorElPopper? false: e.currentTarget)
                                }}
                                classes={{
                                    root: classes.button__root,
                                    label: classes.button__label
                                }}
                            > 
                                Description 
                            </Button>
                            <Popper 
                                open={popper_open} 
                                anchorEl={anchorElPopper}
                                className={classes.popper}    
                                onClick={() => setAnchorElPopper(false)}
                            >
                                <Typography className={classes.text} variant="body2">
                                    { state.description }
                                </Typography>
                            </Popper>
                        </div>
                    </React.Fragment>
                )
            }
        </div>
    )
};


export default Header;