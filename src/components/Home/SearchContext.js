import { useReducer, useState, createContext } from 'react';




const stateReducer = (state, action) => {
    switch(action.type) {
        case "SET_QUERY": {
            return {...state, query: action.data}
        }
        case "SET_DATA": {
            return {...state, data: action.data}
        }
        default: {
            throw new Error(`Non allowed action: ${action.type}`)
        }
    }
};



export const SearchContext = createContext();


export const SearchProvider = ({children}) => {
    const initialState = {
        "query": "",
        "data": []
    };
    const [state, dispatch] = useReducer(stateReducer, initialState);
    return (
        <SearchContext.Provider value={[state, dispatch]}>
            { children }
        </SearchContext.Provider>      
    );
};