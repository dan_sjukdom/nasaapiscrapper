import { useState } from 'react';
import {
    AppBar,
    Typography,
    Toolbar,
    Menu, 
    MenuItem,
    Button,
    useMediaQuery,
    IconButton,
} from '@material-ui/core';
import {
    Link
} from 'react-router-dom';
import {
    makeStyles,
    useTheme
} from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import Sidebar from './Sidebar';



const useStyles = makeStyles(theme => ({
    app_bar__root: {
        backgroundColor: "#212121",
    },
    app_bar__links: {
        display: "flex",
        flexDirection: "row",
    },
    typography__root: {
        fontFamily: "Roboto Mono, monospace",
        flexGrow: 1
    },
    button__root: {
        color: "white",
        backgroundImage: "linear-gradient(to right top, #8358e5, #7a52e6, #704de8, #6547e9, #5842eb)"
    },
    button__label: {
        fontFamily: "Roboto Mono, monospace",
    },
    link__item: {
        textDecoration: "none",
        fontFamily: "Roboto Mono, monospace",
        color: "white"
    },
    menu__list: {
        backgroundColor: "#212121"
    }
}));


const NASA_API = [
    // "Asteroids",
    // "DONKI",
    "Earth",
    // "Exoplanet",
    "Mars"
];



const Navigation = () => {

    const classes = useStyles();
    const theme = useTheme();
    const [showMenu, setShowMenu] = useState(null);
    const [openSideBar, setOpenSideBar] = useState(false);
    const showHMenu = !useMediaQuery(theme.breakpoints.up("sm"))
    const open = Boolean(showMenu);


    const toggleSideBar = (open) => (event) => {
        console.log(event.type)
        console.log(event.key)
        if (event.type === "keydown"  && (event.key === "Tab" || event.key === "Shift")) {
            return;
        }
        setOpenSideBar(open)
    };

    const handleMenuClick = (event) => {
        setShowMenu(event.currentTarget);
    };

    const handleMenuItemClick = (id) => {
        setShowMenu(null);
    };
    

    return (
        <AppBar classes={{root: classes.app_bar__root}} position="sticky">
            <Toolbar>
                <Typography variant="h6" classes={{root: classes.typography__root}}>
                    Planetz
                </Typography>
                <Sidebar 
                    open={openSideBar}
                    handleSideBar={toggleSideBar}
                    menuItems={NASA_API}
                />
                    { 
                        !showHMenu? ( 
                            <div className={classes.app_bar__links}>
                                <Button variant="contained" style={{marginRight: "10px"}} classes={{root: classes.button__root}}>
                                    <Link to="/" className={classes.link__item} style={{color: "white"}}>
                                        Home
                                    </Link>
                                </Button>
                                <Button 
                                    variant="contained" 
                                    classes={{
                                        root: classes.button__root,
                                        label: classes.button__label,
                                    }}
                                    onClick={handleMenuClick}
                                >
                                    Nasa API
                                </Button>
                                <Menu
                                    anchorEl={showMenu} 
                                    open={open} 
                                    onClose={() => setShowMenu(null)}
                                    classes={{
                                        paper: classes.menu__list
                                    }}
                                >
                                    { 
                                        NASA_API.map((item, key) => ( 
                                            <MenuItem 
                                                key={key} 
                                                onClick={ () => handleMenuItemClick(key)}
                                            > 
                                                <Link to={`/nasa/${item.toLowerCase()}`} className={classes.link__item}>    
                                                    {item}  
                                                </Link>
                                            </MenuItem> 
                                        )) 
                                    }
                                </Menu>
                            </div>
                        ) : (
                            <IconButton 
                                edge="start" 
                                aria-label="menu" 
                                onClick={toggleSideBar(true)}    
                            >
                                <MenuIcon style={{ color: "white" }} />
                            </IconButton>
                        )
                    }
            </Toolbar>
        </AppBar>
    )
};

export default Navigation;