import { useState } from 'react';
import {
    Drawer,
    List,
    ListItem,
    ListItemText,
    Collapse
} from '@material-ui/core';
import {
    makeStyles
} from '@material-ui/core/styles';
import {
    Link
} from 'react-router-dom';



const useStyles = makeStyles(theme => ({
    side_bar__paper: {
        backgroundColor: "#2E3440",
    },
    side_bar_list__button: {
        backgroundImage: "linear-gradient(to right, #9f21d6, #8f1ec5, #7f1ab5, #7016a4, #611294)",
        marginBottom: "22px"
    },
    list_item__root: {
        minWidth: "20px",
    },
    list__wrapper: {
        height: "inherit",
    },
    icon: {
        color: "white"
    },
    button__text: {
        textTransform: "uppercase",
        color: "white",
        fontFamily: "Roboto Mono, monospace",
    },
    list_menu__button: {
        backgroundImage: "linear-gradient(to right, #9f21d6, #8f1ec5, #7f1ab5, #7016a4, #611294)",
    },
    list_menu__link: {
        textDecoration: "none",
        color: "white",
        fontFamily: "Roboto Mono, monospace",
    },
    list_menu__text: {
        fontFamily: "Roboto Mono, monospace",
    }
}));




const Sidebar = ({open, handleSideBar, menuItems}) => {
    const classes = useStyles();
    const [ openMenu, setOpenMenu ] = useState(false);
    
    const handleMenuClick = (e) => {
        handleSideBar(false);
    };

    return (
        <Drawer
            open={open}
            classes={{
                paper: classes.side_bar__paper
            }}
        >
            <div
                role="presentation"
                onKeyDown={handleSideBar(false)}
                className={classes.list__wrapper}
            >
                <List>
                    <ListItem 
                        button
                        classes={{
                            button: classes.side_bar_list__button
                        }}
                        onClick={handleSideBar(false)}
                    >
                        <Link to="/" className={classes.list_menu__link}>
                            <ListItemText className={classes.button__text} primary="HOME" />
                        </Link>
                    </ListItem>
                    <ListItem 
                        button
                        classes={{
                            button: classes.side_bar_list__button
                        }}
                        onClick={() => setOpenMenu(!openMenu)}
                    >
                        <ListItemText 
                            className={classes.button__text} 
                            primary="NASA API"
                        />
                    </ListItem>
                    <Collapse
                        in={openMenu}
                        timeout="auto"
                        unmountOnExit
                        onClick={() => setOpenMenu(false)}
                    >
                        <List disablePadding>
                            {
                                menuItems.map((item, key) => (
                                    <ListItem 
                                        button 
                                        key={key} 
                                        className={classes.list_menu__button}
                                        onClick={handleSideBar(false)}
                                    >   
                                        <Link to={`/nasa/${item.toLowerCase()}`} className={classes.list_menu__link}>
                                            <ListItemText primary={item} className={classes.list_menu__text} />
                                        </Link>
                                    </ListItem>
                                ))
                            }
                        </List>
                    </Collapse>
                </List>
            </div>
        </Drawer>
    )
};

export default Sidebar;