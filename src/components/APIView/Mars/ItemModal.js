import { useContext } from 'react';
import {
    Modal,
    makeStyles,
    Typography,
    CircularProgress,
    useMediaQuery,
    useTheme
} from '@material-ui/core';
import { ItemContext } from '../ItemContext';


const useStyles = makeStyles(theme => ({
    modal__root: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    modal__content: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "flex-end",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover"
    },
    modal__text: {
        fontSize: "18px",
        color: "white"
    }
}));


const ItemModal = ({data}) => {
    const classes = useStyles();
    const theme = useTheme();
    const [ state, dispatch ] = useContext(ItemContext);
    const median_screen = !useMediaQuery(theme.breakpoints.up("md"));


    return (
        <Modal
            open={state.openModal} 
            onClose={() => dispatch({ type: "SET_MODAL_OPEN", payload: false })}
            aria-labelledby="item modal"
            aria-describedby="item detailed description modal"
            className={classes.modal__root}
        >
            <div 
                className={classes.modal__content}
                style={median_screen? ({
                    width: "100%",
                    height: "50%",
                    backgroundImage: "url(" + data.img_src + ")",
                }) : ({
                        width: "50%",
                        height: "50%",
                        backgroundImage: "url(" + data.img_src + ")",
                    })
                }
            >
                <Typography variant="h1" className={classes.modal__text}>
                    { data.title }
                </Typography>
                <Typography variant="body2" className={classes.modal__text}>
                    Rover: { data.rover.name }
                </Typography>
                <Typography variant="body2" className={classes.modal__text}>
                    Status: { data.rover.status }
                </Typography>
                <Typography variant="body2" className={classes.modal__text}>
                    Taken: { data.earth_date }
                </Typography>
                <Typography variant="body2" className={classes.modal__text}>
                    Sol: { data.sol }
                </Typography>
            </div>
        </Modal> 
    )
};


export default ItemModal;