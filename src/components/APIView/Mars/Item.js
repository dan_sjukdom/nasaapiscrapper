import {
    useContext
} from 'react';
import {
    Card,
    CardContent,
    CardActionArea,
    Typography,
    makeStyles
} from '@material-ui/core';
import { ItemContext } from '../ItemContext';


const useStyles = makeStyles(theme => ({
    card__image: {
        height: "300px",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "left top",
        animation: "$slider 10s infinite alternate"
    },
    card__body: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        backgroundColor: "#212121"
    },
    card__text: {
        color: "white"
    },
    "@keyframes slider": {
        "from": {
            backgroundPosition: "left top"
        },
        "to": {
            backgroundPosition: "rigth bottom"
        }
    }
}));


const Item = ({data}) => {
    const classes = useStyles();
    const [state, dispatch] = useContext(ItemContext);

    const handleClick = () => {
        dispatch({
            type: "SET_DETAIL_ITEM",
            payload: data
        })
        dispatch({
            type: "SET_MODAL_OPEN",
            payload: true
        });
   }

    return (
        <Card>
            <CardActionArea onClick={handleClick}>
                <div 
                    className={classes.card__image} 
                    style={{
                        backgroundImage: "url(" + data.img_src + ")",
                    }}
                > </div>
                <CardContent className={classes.card__body}>
                    <Typography variant="body1" className={classes.card__text}>
                        Rover: {data.rover.name}
                    </Typography>
                    <Typography variant="body1" className={classes.card__text}>
                        Status: {data.rover.status}
                    </Typography>
                    <Typography variant="body1" className={classes.card__text}>
                        Taken: {data.earth_date}
                    </Typography>
                    <Typography variant="body1" className={classes.card__text}>
                        Sol: {data.sol}
                    </Typography>
                    <Typography variant="body1" className={classes.card__text}>
                        Camera: {data.camera.name}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    )
};


export default Item;