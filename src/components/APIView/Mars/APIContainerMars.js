import React, { 
    useEffect, 
    useContext
} from 'react';
import {
    Grid,
    CircularProgress,
    Typography,
} from '@material-ui/core';
import {
    makeStyles
} from '@material-ui/core/styles';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import axios from 'axios';
import { API_KEY } from '../../../helpers/api';
import Item from './Item';
import ItemModal from './ItemModal';
import { ItemContext } from '../ItemContext';





const useStyles = makeStyles(theme => ({
    main__grid: {
        marginTop: "30px"
    },
    loading_component: {
        position: "absolute",
        left: "50%",
        top: "50%",
        transform: "translate(-50%, -50%)"
    },
    failed__status: {
        position: "absolute",
        left: "50%",
        top: "50%",
        transform: "translate(-50%, -50%)",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    failed__text: {
        fontFamily: "Roboto Mono, monospace",
        color: "white"
    },
    sad__icon: {
        color: "white",
        width: "100px",
        height: "100px"
    }
}));


const APIContainer = ({type}) => {
    const classes = useStyles();
    const [ state, dispatch ] = useContext(ItemContext);

    const url = `https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=${state.MARS_SOL}&camera=${state.MARS_CAMERA}&api_key=${API_KEY}`


    const fetchData = async (url, page) => {
        dispatch({
            type: "SET_LOADING",
            payload: true
        });
        dispatch({
            type: "SET_DATA",
            payload: []
        });
        try {
            const response = await axios.get(url+`&page=${page}`);
            if (response.status === 200) {
                const data = response.data.photos;
                console.log(response)
                dispatch({
                    type: "SET_DATA",
                    payload: data
                });
                dispatch({
                    type: "SET_LOADING",
                    payload: false
                });
                dispatch({
                    type: "SET_TOTAL_PAGES",
                    payload: 10
                })
            }
            else {
                console.log("The request wasn't successful");
            }
        }
        catch (error) {
            throw new Error(error);
        }
    } ;

    useEffect(() => {
        if (url) {
            fetchData(url, state.current_page);
        }
    }, [state.current_page, state.MARS_SOL, state.MARS_CAMERA]);

    return (
        <div>
            {
                state.loading && state.data.length === 0? (
                    <div className={classes.loading_component}>
                        <CircularProgress
                            color="secondary"
                        />
                    </div>
                ) : state.data.length>0 ? (
                    <div>
                        { 
                            state.openModal? <ItemModal data={state.item_detail} /> : ""
                        }
                        <Grid container spacing={3} className={classes.main__grid}>
                            {
                                state.data.map((item) => (
                                    <Grid item key={item.id} xs={12} sm={6} lg={4}>
                                        <Item data={item} />
                                    </Grid>
                                ))
                            }
                        </Grid>
                    </div>
                ) : (
                    <div className={classes.failed__status}>
                        <Typography variant="body2" className={classes.failed__text}> 
                            There are no pictures for this camera/sol
                        </Typography>
                        <SentimentVeryDissatisfiedIcon className={classes.sad__icon} />
                    </div>
                )
            }
        </div>
    )
};


export default APIContainer;