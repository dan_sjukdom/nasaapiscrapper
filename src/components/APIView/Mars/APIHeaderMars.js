import { useContext, useState } from 'react';
import {
    Select,
    MenuItem,
    InputLabel,
    FormControl,
    makeStyles,
    TextField
} from '@material-ui/core';
import { ItemContext } from '../ItemContext';



const useStyles = makeStyles(theme => ({
    root: {
        marginTop: "50px"
    },
    form__root: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    input__root: {   
    }
}));


const cameras = [
    "fhaz",
    "rhaz",
    "mast",
    "chemcam",
    "mahli",
    "mardi",
    "navcam",
];


const APIHeaderMars = () => {

    const classes = useStyles();
    const [ state, dispatch ] = useContext(ItemContext);

    const handleMartianSolInput = (e) => {
        let sol = e.target.value;
        if (sol.match(/^\d+$/g)) {
            sol = Number(sol);
            if (sol >= 0 && sol <= 1000) {
                dispatch({
                    type: "SET_MARS_SOL",
                    payload: sol
                })
            };
        }
        else if (sol === "") {
            dispatch({
                type: "SET_MARS_SOL",
                payload: ""
            })
        };
    };

    const handleCameraQuery = (e) => {
        const camera = e.target.value;
        dispatch({
            type: "SET_MARS_CAMERA",
            payload: camera
        })
    };

    return (
        <div className={classes.root}>
            <FormControl classes={{
                root: classes.form__root
            }}>
                <TextField
                    label="cameras"
                    value={state.MARS_CAMERA}
                    onChange={handleCameraQuery}
                    select
                >
                    {
                        cameras.map((item, key) => (
                            <MenuItem key={key} value={item}> {item} </MenuItem>
                        ))
                    }
                </TextField>
                <TextField
                    label="Martian Sol"
                    id="input-martian-sol"
                    value={state.MARS_SOL}
                    onChange={handleMartianSolInput}
                    type="number"
                />
            </FormControl>
        </div>
    )
};


export default APIHeaderMars;