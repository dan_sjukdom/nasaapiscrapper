import { createContext, useReducer } from 'react';




export const ItemContext = createContext();


const reducer = (state, action) => {
    switch(action.type) {
        case "SET_DATA": {
            return {
                ...state,
                data: action.payload
            }
        }
        case "SET_MODAL_OPEN": {
            return {
                ...state,
                openModal: action.payload
            }
        }
        case "SET_LOADING": {
            return {
                ...state,
                loading: action.payload,
            }
        }
        case "SET_DETAIL_ITEM": {
            return {
                ...state,
                item_detail: action.payload
            }
        } 
        case "SET_CURRENT_PAGE": {
            return {
                ...state,
                current_page: action.payload
            }
        }
        case "SET_TOTAL_PAGES": {
            return {
                ...state,
                total_pages: action.payload
            }
        }
        case "SET_MARS_SOL": {
            return {
                ...state,
                MARS_SOL: action.payload
            }
        }
        case "SET_MARS_CAMERA": {
            return {
                ...state,
                MARS_CAMERA: action.payload
            }
        }
        case "SET_EARTH_DATE": {
            return {
                ...state,
                EARTH_DATE: action.payload
            }
        }
        case "SET_EARTH_LAT": {
            return {
                ...state,
                EARTH_LAT: action.payload
            }
        }
        case "SET_EARTH_LON": {
            return {
                ...state,
                EARTH_LON: action.payload
            }
        }
        case "SET_EARTH_DIM": {
            return {
                ...state,
                EARTH_DIM: action.payload
            }
        }
        default: {
            throw new Error("Unexpected action")
        }
    };
}; 


export const ItemContextProvider = ({children}) => {

    const [state, dispatch] = useReducer(reducer, {
        data: [],
        openModal: false,
        loading: true,
        item_detail: {},
        current_page: 1,
        total_pages: 1,
        MARS_SOL: 999,
        MARS_CAMERA: "mast",
        EARTH_DATE: "2020-10-10",
        EARTH_LAT: 37.78,
        EARTH_LON: -82.33,
        EARTH_DIM: 0.15
    });

    return (
        <ItemContext.Provider value={[state, dispatch]}>
            { children }
        </ItemContext.Provider>
    )
};