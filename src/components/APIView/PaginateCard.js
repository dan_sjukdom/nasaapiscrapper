import {
    useContext
} from 'react';
import {
    makeStyles
} from '@material-ui/core';
import {
    Pagination
} from '@material-ui/lab';
import { ItemContext } from './ItemContext';


const useStyles = makeStyles(theme => ({
    root: {
        marginTop: "30px",
        marginBottom: "30px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100px"
    }
}));


const PaginateCard = () => {
    const classes = useStyles();
    const [ state, dispatch ] = useContext(ItemContext);

    const handleChange = (event, value) => {
        dispatch({
            type: "SET_CURRENT_PAGE",
            payload: value
        });
        window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth"
        })
    }

    return (
        <div>
            { 
                state.loading? "" : ( 
                        <Pagination
                            className={classes.root}
                            defaultPage={state.current_page}
                            count={state.total_pages}
                            color="secondary"
                            onChange={handleChange}
                            size="medium"
                        />
                )
            }
        </div>
    )
};


export default PaginateCard;