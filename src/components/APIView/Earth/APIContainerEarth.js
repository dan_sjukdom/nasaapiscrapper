import { useContext, useEffect, useState } from 'react';
import { ItemContext } from '../ItemContext';
import { API_KEY } from '../../../helpers/api';
import axios from 'axios';
import { 
    CircularProgress,
    Snackbar,
    makeStyles
} from '@material-ui/core';
import {
    Alert
} from '@material-ui/lab';


const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    image__root: {
        width: "100vw",
        height: "100vh",
        marginTop: "50px"
    },
    loading__component: {
        position: "absolute",
        left: "50%",
        top: "50%",
        transform: "translate(-50%, -50%)"
    }
}));



const APIContainerEarth = () => {

    const classes = useStyles();
    const [ state, dispatch ] = useContext(ItemContext);
    const [ error, setError ] = useState(false);

    const base_url = `https://api.nasa.gov/planetary/earth/assets?lon=${state.EARTH_LON}&lat=${state.EARTH_LAT}&dim=${state.EARTH_DIM}&date=${state.EARTH_DATE}&api_key=${API_KEY}`;

    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }
        setError(false);
    };

    const fetchEarthImage = async (url) => {
        dispatch({
            type: "SET_LOADING",
            payload: true
        });
        dispatch({
            type: "SET_DATA",
            payload: []
        })
        try {
            const res = await axios.get(url);
            if (res.status === 200) {
                const image = res.data.url;
                dispatch({
                    type: "SET_DATA",
                    payload: [image]
                });
                dispatch({
                    type: "SET_LOADING",
                    payload: false
                });
                setError(false);
                
            }
            else {
                setError(true);
                dispatch({
                    type: "SET_LOADING",
                    payload: false
                });
            }
        }
        catch (err) {
            setError(true);
            dispatch({
                type: "SET_LOADING",
                payload: false
            });
        }
    };

    useEffect(() => {
        fetchEarthImage(base_url);
    }, [state.EARTH_LAT, state.EARTH_LON]);

    return (
        <div className={classes.root}>
            {
                state.loading? (
                    <div className={classes.loading__component}>
                        <CircularProgress color="secondary" />
                    </div>
                ) : error? (
                    <Snackbar
                        anchorOrigin={{
                            vertical:"top", 
                            horizontal: "center"
                        }}  
                        open={error} autoHideDuration={10000} 
                        onClose={handleClose}
                    >
                        <Alert 
                            severity="warning" 
                            onClose={handleClose}
                        >
                            There're no images for the selected date
                        </Alert>
                    </Snackbar>
                ) : (
                    <div>
                        <img src={state.data[0]} alt="" className={classes.image__root}/>
                    </div>
                )
            }
        </div>
    )
};

export default APIContainerEarth;