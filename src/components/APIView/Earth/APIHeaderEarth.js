import { useContext, useState } from "react";
import {
    FormControl,
    Button,
    makeStyles,
    TextField,
    Typography
} from "@material-ui/core";
import { ItemContext } from "../ItemContext";


const useStyles = makeStyles(theme => ({
    root: {
        marginTop: "50px",
    },
    form__root: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-evenly",
        alignItems: "center"
    },
    text__input: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    button__root: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundImage: "linear-gradient(to right top, #8358e5, #7a52e6, #704de8, #6547e9, #5842eb)",
        marginTop: "30px"
    },
    text__root: {
        fontFamily: "Roboto Mono, monospace",
        textAlign: "center",
        marginBottom: "30px",
        color: "white"
    }
}));


const LONLAT_PATTERN = /^(?:-)?\d+(?:\.\d+)?$/;


const APIHeaderEarth = () => {
    
    const classes = useStyles();
    const [state, dispatch] = useContext(ItemContext);
    const [earth_lat, setEarthLat] = useState(state.EARTH_LAT);
    const [earth_lon, setEarthLon] = useState(state.EARTH_LON);
    const [latError, setLatError] = useState(false);
    const [lonError, setLonError] = useState(false);

    const handleEarthLon = (e) => {
        setEarthLon(e.target.value);
    };

    const handleEarthLat = (e) => {
        setEarthLat(e.target.value);
    };

    const handleSubmit = () => {
        const lat_match = LONLAT_PATTERN.test(String(earth_lat));
        const lon_match = LONLAT_PATTERN.test(String(earth_lon));
        if (!lat_match) {
            setLatError(true);
        }
        else {
            setLatError(false);
        }
        if (!lon_match) {
            setLonError(true);
        }
        else {
            setLonError(false);
        }
        if (latError === false && lonError === false) {
            dispatch({
                type: "SET_EARTH_LAT",
                payload: earth_lat
            });
            dispatch({
                type: "SET_EARTH_LON",
                payload: earth_lon
            });
        }
    };
 
    return (
        <div className={classes.root}>
            <Typography variant="body2" className={classes.text__root}>
                The satellites that capture the pictures passes over each coordinate point 
                in earth once every sixteen days in average. Note that some images may have an area 
                that has transparecy.
            </Typography>
            <FormControl classes={{
                root: classes.form__root
            }}>
                <div className={classes.text__input}>
                    <TextField
                        label="Latitude"
                        type="number"
                        value={earth_lat}
                        onChange={handleEarthLat}
                        error={latError}
                        helperText={latError ? "Make sure the value's a valid float number" : null}
                        style={{
                            marginBottom: "10px"
                        }}
                    />
                    <TextField
                        label="Longitude"
                        type="number"
                        value={earth_lon}
                        onChange={handleEarthLon}
                        error={lonError}
                        helperText={lonError? "Make sure the value's a valid float number" : null}
                        style={{
                            marginBottom: "10px"
                        }}
                    />
                </div>
                <Button 
                    className={classes.button__root}
                    variant="contained"
                    color="secondary"
                    onClick={handleSubmit}
                >
                    Submit
                </Button>
            </FormControl>
        </div>
    )
};


export default APIHeaderEarth;