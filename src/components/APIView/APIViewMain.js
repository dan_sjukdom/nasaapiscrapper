import {
    Container,
    makeStyles
} from '@material-ui/core';
import APIContainerMars from './Mars/APIContainerMars';
import APIHeaderMars from './Mars/APIHeaderMars';
import APIHeaderEarth from './Earth/APIHeaderEarth';
import APIContainerEarth from './Earth/APIContainerEarth';
import { ItemContextProvider } from './ItemContext';
import PaginateCard from './PaginateCard';


const useStyles = makeStyles(theme => ({

}));


const api_header = {
    "mars": <APIHeaderMars />,
    "earth": <APIHeaderEarth />
};


const APIViewMain = ({match}) => {

    const type = match.params.type;
    const classes = useStyles();

    return (
        <Container>
            <ItemContextProvider>
                { api_header[type] }
                {
                    type === "earth"? <APIContainerEarth /> : type === "mars" ? <APIContainerMars /> : null
                }
                {/* {
                    ["mars"].includes(type)? <PaginateCard /> : ""
                } */}
            </ItemContextProvider>
        </Container>
    )
};


export default APIViewMain;