import {
  BrowserRouter as Router,
  HashRouter as HRouter,
  Route,
  Switch,
} from 'react-router-dom';
import Navigation from './components/Navigation';
import Home from './components/Home/Home';
import APIViewMain from './components/APIView/APIViewMain';
import {
  Container,
} from '@material-ui/core';
import "./assets/css/App.css";


function App() {
  return (
    <HRouter>
      <Navigation />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/nasa/:type" component={APIViewMain} />
      </Switch>
    </HRouter>
  );
}

export default App;